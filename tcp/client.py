import pickle
import socket

from tcp.server import RequestHandler


# ======================================================================================================================
class TCPClient(object):
    CMD_DISCONNECT = RequestHandler.CMD_DISCONNECT

    MSG_IN_LEN  = RequestHandler.MSG_IN_LEN
    MSG_OUT_LEN = RequestHandler.MSG_OUT_LEN

    MSG_OUT_FORMAT = '%' + str(MSG_OUT_LEN) + 'd%s'

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, host, port, sock=None):
        self.host = host
        self.port = port

        if sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock

    # ------------------------------------------------------------------------------------------------------------------
    def _connect(self, cmd=None):
        try:
            self.sock.connect((self.host, self.port))
            if cmd is not None:
                return self._exec_cmd(cmd)
            else:
                return None
        except socket.error:
            print('Error: Cannot connect to the server (%s)' % self.__class__.__name__)
            return None

    # ------------------------------------------------------------------------------------------------------------------
    def _exec_cmd(self, cmd, do_decode=False):
        try:
            self._send(cmd)
            return self._recv(do_decode)
        except socket.error:
            return None

    # ------------------------------------------------------------------------------------------------------------------
    def _exec_cmd_get_pickle(self, cmd):
        res = ''
        try:
            res = self._exec_cmd(cmd, False)
            return pickle.loads(res)
        except pickle.UnpicklingError as e:
            print('Unpickling error: %s  %s' % (e, res))
            return None
        except EOFError or socket.error:
            return None

    # ------------------------------------------------------------------------------------------------------------------
    def _recv(self, do_decode=False):
        msg = self.sock.recv(self._recv_msg_len())
        return msg.decode() if (do_decode) else msg

    # ------------------------------------------------------------------------------------------------------------------
    def _recv_msg_len(self):
        try:
            return int(self.sock.recv(self.MSG_OUT_LEN))
        except ValueError:
            return 0

    # ------------------------------------------------------------------------------------------------------------------
    def _send(self, msg):
        self.sock.send(str(self.MSG_OUT_FORMAT % (len(msg), msg)).encode())

    # ------------------------------------------------------------------------------------------------------------------
    def connect(self):
        self._connect()

    # ------------------------------------------------------------------------------------------------------------------
    def disconnect(self):
        try:
            self._send(self.CMD_DISCONNECT)
        except socket.error:
            pass
        finally:
            self.sock.close()
