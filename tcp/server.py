# ----------------------------------------------------------------------------------------------------------------------
#
# Example telnet usage
#     |telnet <host> <port>
#     |       4ping
#     |      7echo hi
#     |      10disconnect
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources
#     pymotw.com/2/SocketServer
#     docs.python.org/2/library/socketserver.html
#     docs.python.org/3.3/library/socketserver.html
#
# ----------------------------------------------------------------------------------------------------------------------

import socket
import socketserver


# ======================================================================================================================
class RequestHandler(socketserver.BaseRequestHandler):
    CMD_DELIM = ' '

    CMD_DISCONNECT = 'disconnect'

    RET_OK      = '0'
    RET_ERR     = '1'
    RET_CMD_ERR = '2'

    MSG_IN_LEN  = 8  # number of characters containing the size of command
    MSG_OUT_LEN = 8  # number of characters containing the size of response

    MSG_OUT_FORMAT = '%' + str(MSG_OUT_LEN) + 'd%s'  # format of the response

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, request, client_address, server):
        socketserver.BaseRequestHandler.__init__(self, request, client_address, server)

        self.server = server

    # ------------------------------------------------------------------------------------------------------------------
    def cmd_echo(self, s):
        return s

    # ------------------------------------------------------------------------------------------------------------------
    def cmd_ping(self):
        return 'pong'

    # ------------------------------------------------------------------------------------------------------------------
    def do_cmd(self, cmd, args):
        return {
            'ping': lambda: self.cmd_ping(),        # cmd:       4ping
            'echo': lambda: self.cmd_echo(args[0])  # cmd:      10echo hello
        }.get(cmd, lambda: self.RET_CMD_ERR)()      # unknown command

    # ------------------------------------------------------------------------------------------------------------------
    def handle(self):
        while True:
            cmd = self.request.recv(self.MSG_IN_LEN).decode().strip()
            if not cmd: continue

            try:
                msg_len = int(cmd)
            except Exception:
                continue

            cmd = self.request.recv(msg_len).decode().strip()
            cmd = cmd.split(self.CMD_DELIM)

            if cmd[0] == self.CMD_DISCONNECT:  # cmd:      10disconnect
                break

            if self.server.do_ret:
                res = self.do_cmd(cmd[0], cmd[1:])
                self.request.send(str(self.MSG_OUT_FORMAT % (len(res), res)).encode())
            else:
                self.do_cmd(cmd[0], cmd[1:])


# ======================================================================================================================
class TCPServer(socketserver.TCPServer):

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True, do_ret=False):
        socketserver.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate)

        self.do_ret = do_ret

    # ------------------------------------------------------------------------------------------------------------------
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)


# ======================================================================================================================
if __name__ == '__main__':
    import signal
    import sys

    s = TCPServer(('127.0.0.1', 9999), RequestHandler)

    signal.signal(signal.SIGINT, lambda signal, frame: sys.exit(0))  # register CTRL+C event

    s.serve_forever()
