import tcp.server


# ======================================================================================================================
class TestRequestHandler(tcp.server.RequestHandler):

    # ------------------------------------------------------------------------------------------------------------------
    def do_cmd(self, cmd, args):
        return {
            'len' : lambda: str(len(args[0])),
            '2x'  : lambda: str(float(args[0]) * 2)
        }.get(cmd, lambda: '')()


# ======================================================================================================================
class TestServer(tcp.server.TCPServer):
    PORT = 3333

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True):
        tcp.server.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate, do_ret=True)


# ======================================================================================================================
if __name__ == '__main__':
    import signal
    import sys

    s = TestServer(('localhost', TestServer.PORT), TestRequestHandler)

    signal.signal(signal.SIGINT, lambda signal, frame: sys.exit(0))  # register CTRL+C event

    s.serve_forever()
