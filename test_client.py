import tcp.client


# ======================================================================================================================
class TestClient(tcp.client.TCPClient):

    # ------------------------------------------------------------------------------------------------------------------
    def cmd_len(self, s): print(self._exec_cmd('len {}'.format(s), True))
    def cmd_2x (self, f): print(self._exec_cmd('2x {}' .format(f), True))


# ======================================================================================================================
if __name__ == '__main__':
    import math
    import test_server

    c = TestClient('localhost', test_server.TestServer.PORT)
    c.connect()
    c.cmd_len('test')
    c.cmd_2x(math.pi)
    c.disconnect()
