# TCP.py
A TCP client and server written in Python.  It should be customised to specific needs.


## Dependencies
- [Python 3](https://www.python.org)


## Setup
First, initialize a new Python venv:
```sh
venv=tcp.py
python=python3.6

mkdir -p ~/prj/
cd ~/prj
$python -m venv $venv
cd $venv
source ./bin/activate

mkdir -p src
cd src
git clone https://github.com/ubiq-x/tcp.py
```

Then, start the server:
```sh
cd tcp.py
python test_server.py
```

Finally, connect to the server from another terminal session:
```sh
venv=tcp.py
cd ~/prj/$venv
source ./bin/activate
cd src/tcp.py
python test_client.py
```

You can also telnet to the server.  Here's how to do that with the default server (i.e., `Server.py`, not `test_server.py`):
```sh
telnet <host> <port>
       4ping
      7echo hi
      10disconnect
```
Note that leading spaces are important and, along with message length, reflect the maximum message length expected by the server (8 bytes for the default server).
